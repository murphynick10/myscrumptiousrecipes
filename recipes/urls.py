from django.urls import path
from recipes.views import recipe_list, show_recipe, create_recipe, edit_recipe, my_recipe_list

urlpatterns = [
    path("", recipe_list, name="all_recipes"),
    path("<int:id>/", show_recipe, name="recipe_detail"),
    path("create/", create_recipe, name="create_recipe"),
    path("edit/<int:id>/", edit_recipe, name="recipe_edit"),
    path("mine/", my_recipe_list, name="my_recipe_list"),
]
