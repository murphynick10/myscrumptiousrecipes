from django.shortcuts import render, get_object_or_404
from .models import Recipe
from .forms import RecipeForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required


def show_recipe(request, id):
    a_really_cool_recipe = get_object_or_404(Recipe, id=id)
    context = {"recipe_object": a_really_cool_recipe}
    return render(request, "recipes/detail.html", context)


def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {"recipe_list": recipes}
    return render(request, "recipes/list.html", context)


@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)


@login_required
def create_recipe(request):
    if request.method == "POST":
        # We should use the form to validate the values
        #   and save them to the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            form.save()
            # If all goes well, we can redirect the browser
            #   to another page and leave the function
            return redirect("all_recipes")
    else:
        # Create an instance of the Django model form class
        form = RecipeForm()

    # Put the form in the context
    context = {
        "form": form,
    }
    # Render the HTML template with the form
    return render(request, "recipes/create.html", context)


def edit_recipe(request, id):
    post = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=post)
        if form.is_valid():
            form.save()

            return redirect("recipe_detail", id=id)
    else:

        form = RecipeForm(instance=post)

    context = {
        "post_object": post,
        "form": form,
    }

    return render(request, "recipes/create.html", context)
